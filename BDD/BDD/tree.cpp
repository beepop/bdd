//
//  tree.cpp
//  BDD
//
//  Created by Branko Popovic on 8/16/14.
//  Copyright (c) 2014 Branko Popović. All rights reserved.
//

#include "tree.h"
#include <iostream>
#include <queue>
#include <vector>
#include <math.h>

using namespace std;

binary_tree::binary_tree()
{
    root = nullptr;
};

binary_tree::~binary_tree()
{
    binary_tree::destroy_tree();
};

void binary_tree::destroy_tree(node *leaf)
{
    if (leaf != nullptr)
    {
        destroy_tree(leaf->left);
        destroy_tree(leaf->right);
        delete leaf;
    }
};

void binary_tree::insertBool(bool *value)
{
    std::vector<node*>::iterator it = this->rootHistory.end()-1;
    node * newRoot = *it;
    
    if (newRoot->leftBoolValue == nullptr)
    {
        newRoot->leftBoolValue = value;
    }
    else if (newRoot->rightBoolValue == nullptr)
    {
        newRoot->rightBoolValue = value;
    }
    
}

void binary_tree::insert(int key, node *leaf)
{
    
    if(key < leaf->key_value)
    {
        if(leaf->left!= nullptr)
            insert(key, leaf->left);
        else
        {
            leaf->left=new node;
            leaf->left->key_value=key;
            leaf->left->left= nullptr;
            leaf->left->right= nullptr;
            leaf->left->leftBoolValue = nullptr;
            leaf->left->rightBoolValue = nullptr;
            this->rootHistory.push_back(leaf->left);
        }
    }
    else if(key>=leaf->key_value)
    {
        if(leaf->right!= nullptr)
            insert(key, leaf->right);
        else
        {
            leaf->right=new node;
            leaf->right->key_value=key;
            leaf->right->left= nullptr;
            leaf->right->right= nullptr;
            leaf->right->rightBoolValue = nullptr;
            leaf->right->leftBoolValue = nullptr;
            this->rootHistory.push_back(leaf->right);
        }
    }

};

node *binary_tree::search(int key, node *leaf)
{
    if(leaf!=nullptr)
    {
        if(key==leaf->key_value)
            return leaf;
        if(key<leaf->key_value)
            return search(key, leaf->left);
        else
            return search(key, leaf->right);
    }
    else return nullptr;
}

void binary_tree::insert(int key)
{
    if(root!=nullptr)
    {
        std::vector<node*>::iterator it = this->rootHistory.end()-1;
        node * newRoot = *it;
        
        if (newRoot->left != nullptr && newRoot->right != nullptr)
        {
            updateRootHistory();
            std::vector<node*>::iterator it = this->rootHistory.end()-1;
            newRoot = *it;
        }
        
        
        cout << "ROOT " << newRoot->key_value << endl;
        insert(key, newRoot);
    }
    else
    {
        root=new node;
        root->key_value=key;
        root->left=nullptr;
        root->right=nullptr;
        this->rootHistory.push_back(root);
    }
};

node *binary_tree::search(int key)
{
    return search(key, root);
};

void binary_tree::destroy_tree()
{
    destroy_tree(root);
};

void binary_tree::print(node *leaf)
{
    /*if (leaf != nullptr)
    {
        cout << leaf->key_value << " ";
        print(leaf->left);
        print(leaf->right);
    }
    else
    {
        cout << "null ";
    }*/
    
    queue<node*> currentLevel, nextLevel;
    currentLevel.push(root);
    while (!currentLevel.empty()) {
        node *currNode = currentLevel.front();
        currentLevel.pop();
        if (currNode) {
            cout << currNode->key_value << " ";
            nextLevel.push(currNode->left);
            nextLevel.push(currNode->right);
        }
        if (currentLevel.empty()) {
            cout << endl;
            swap(currentLevel, nextLevel);
        }
    }
    

};

void printLevelOrder(node *root) {
    if (!root) return;
    queue<node*> nodesQueue;
    int nodesInCurrentLevel = 1;
    int nodesInNextLevel = 0;
    nodesQueue.push(root);
    while (!nodesQueue.empty()) {
        node *currNode = nodesQueue.front();
        nodesQueue.pop();
        nodesInCurrentLevel--;
        if (currNode) {
            cout << currNode->key_value << " ";
            nodesQueue.push(currNode->left);
            nodesQueue.push(currNode->right);
            nodesInNextLevel += 2;
        }
        if (nodesInCurrentLevel == 0) {
            cout << endl;
            nodesInCurrentLevel = nodesInNextLevel;
            nodesInNextLevel = 0;
        }
    }
}

void binary_tree::print()
{
    if (root!=nullptr)
    {
        print(root);
    }
};

bool binary_tree::isTreeEmpty()
{
    if (root==nullptr)
        return true;
    
    return false;
}

void binary_tree::updateRootHistory()
{
    if (this->rootHistory.size() == 0)
    {
        return;
    }
    
    this->rootHistory.pop_back();
}

void binary_tree::searchValuation(std::vector<int> valuation, bool * isRootFound, node * startRoot)
{
    if(startRoot == nullptr)
    {
        startRoot = root;
    }
    
    for (int i=0; i<valuation.size();i++)
    {
        int test = valuation[i];
        
        if (test < 0 && startRoot->leftBoolValue != nullptr && abs(startRoot->key_value) == abs(test))
        {
            cout << "EVAL (left) -> " << *startRoot->leftBoolValue << endl;
            return;
        }
        else if (test > 0 && startRoot->rightBoolValue != nullptr && abs(startRoot->key_value) == abs(test)) {
            cout << "EVAL (right) -> " << *startRoot->rightBoolValue << endl;
            return;
        }
        else
        {
            if(abs(startRoot->key_value) == abs(test))
            {
                //*isRootFound = true;
                
                valuation.erase(std::remove(valuation.begin(), valuation.end(), test));
                
                if (test < 0)
                {
                    searchValuation(valuation, isRootFound, startRoot->left);
                }
                else
                {
                    searchValuation(valuation, isRootFound, startRoot->right);
                }
                
            }
        }
    }
}


node * binary_tree::getRoot()
{
    return root;
}
