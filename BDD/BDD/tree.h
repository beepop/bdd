//
//  tree.h
//  BDD
//
//  Created by Branko Popovic on 8/16/14.
//  Copyright (c) 2014 Branko Popović. All rights reserved.
//

#include <vector>

#ifndef BDD_tree_h
#define BDD_tree_h



struct node
{
    int key_value;
    bool * leftBoolValue;
    bool * rightBoolValue;
    node * left;
    node * right;
};

class binary_tree
{
public:
    binary_tree();
    ~binary_tree();
    
    void insert(int key);
    node *search(int key);
    void destroy_tree();
    bool isTreeEmpty();
    void print();
    void updateRootHistory();
    void insertBool(bool * value);
    void searchValuation(std::vector<int> valuation, bool * isRootFound, node * startRoot = nullptr);
    node * getRoot();
private:
    node * root;
    std::vector<node*> rootHistory;
    
    void destroy_tree(node *leaf);
    void insert(int key, node *leaf);
    void insertLeft(int key, node *leaf);
    void insertRight(int key, node *leaf);
    node * search(int key, node *leaf);
    void print(node *leaf);
};


#endif
