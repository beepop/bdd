//
//  main.cpp
//  BDD
//
//  Created by Branko Popović on 6/22/14.
//  Copyright (c) 2014 Branko Popović. All rights reserved.
//
#include <iostream>
#include <set>
#include <map>
#include <vector>
#include <memory>
#include <algorithm>
#include <iterator>

#include "tree.h"

using namespace std;

/* Skup atoma (iskaznih promenljivih) */
typedef set<unsigned> AtomSet;

/* Valuacija je predstavljena mapom koja iskaznim promenljivama
 (predstavljenim unsigned tipom) dodeljuje bool vrednosti */
class Valuation {
private:
    map<unsigned, bool> _map;
public:
    
    void setInitial(const AtomSet & atoms);
    bool nextValuation();
    
    bool getValue(unsigned p) const;
    void setValue(unsigned p, bool b);
    
    void printValuation() const;
};

/* Deklaracija osnovne klase */
class BaseFormula;

typedef shared_ptr<BaseFormula> Formula;

typedef vector<Formula> LiteralList;
typedef vector<LiteralList> LiteralListList;

/* Apstraktna klasa za predstavljanje formula */
class BaseFormula : public enable_shared_from_this<BaseFormula> {
    
public:
    /* Tip formule (tj. vodeceg veznika formule) */
    enum Type { T_TRUE, T_FALSE, T_ATOM, T_NOT, T_AND, T_OR, T_IMP, T_IFF };
    
    virtual void printFormula() const = 0;
    virtual Type getType() const = 0;
    virtual unsigned complexity() const = 0;
    virtual bool equalTo(const Formula & f) const = 0;
    virtual void getAtoms(AtomSet & set) const = 0;
    virtual bool eval(const Valuation & v) const = 0;
    virtual Formula substitute(const Formula & a, const Formula & b) = 0;
    virtual Formula simplify() = 0;
    virtual Formula nnf() = 0;
    virtual void shannon() = 0;
    virtual LiteralListList listCNF() = 0;
    virtual LiteralListList listDNF() = 0;
    virtual ~BaseFormula() {}
    
    virtual bool containsFormula(const Formula & f) const = 0;
    
    void printTruthTable() const;
    bool isSatisfiable(Valuation & v) const;
    bool isTautology() const;
};

/* Atomicke formule (True, False i Atom) */
class AtomicFormula : public BaseFormula {
    
public:
    virtual unsigned complexity() const;
    virtual Formula substitute(const Formula & a, const Formula & b);
    virtual Formula simplify();
    virtual Formula nnf();
    virtual void shannon();
    
};

/* Logicke konstante (True i False) */
class LogicConstant : public AtomicFormula {
    
public:
    virtual bool equalTo(const Formula & f) const;
    virtual void getAtoms(AtomSet & set) const;

};

class True : public LogicConstant {
    
public:
    virtual void printFormula() const;
    virtual Type getType() const;
    virtual bool eval(const Valuation & v) const;
    virtual LiteralListList listCNF();
    virtual LiteralListList listDNF();
    virtual bool containsFormula(const Formula & f) const;
};

class False : public LogicConstant {
    
public:
    virtual void printFormula() const;
    virtual Type getType() const;
    virtual bool eval(const Valuation & v) const;
    virtual LiteralListList listCNF();
    virtual LiteralListList listDNF();
    virtual bool containsFormula(const Formula & f) const;
};

class Atom : public AtomicFormula {
private:
    unsigned _var_num;
public:
    Atom(unsigned num);
    unsigned getVariableNumber() const;
    virtual void printFormula() const;
    virtual Type getType() const;
    virtual bool equalTo(const Formula &  f) const;
    virtual void getAtoms(AtomSet & set) const;
    virtual bool eval(const Valuation & v) const;
    virtual LiteralListList listCNF();
    virtual LiteralListList listDNF();
    virtual bool containsFormula(const Formula & f) const;
};

/* Unarni veznik (obuhvata samo Not) */
class UnaryConjective : public BaseFormula {
protected:
    Formula _op;
public:
    UnaryConjective(const Formula & op);
    const Formula & getOperand() const;
    virtual unsigned complexity() const;
    virtual bool equalTo(const Formula & f) const;
    virtual void getAtoms(AtomSet & set) const;

    
};

class Not : public UnaryConjective {
public:
    
    Not(const Formula & op);
    virtual void printFormula() const;
    virtual Type getType() const;
    virtual bool eval(const Valuation & v) const;
    virtual Formula substitute(const Formula & a, const Formula & b);
    virtual Formula simplify();
    virtual Formula nnf();
    virtual void shannon();
    virtual LiteralListList listCNF();
    virtual LiteralListList listDNF();
    virtual bool containsFormula(const Formula & f) const;
    
};

/* Binarni veznik (And, Or, Imp i Iff) */
class BinaryConjective : public BaseFormula {
protected:
    Formula _op1;
    Formula _op2;
public:
    BinaryConjective(const Formula & op1, const Formula & op2);
    const Formula & getOperand1() const;
    const Formula & getOperand2() const;
    virtual unsigned complexity() const;
    virtual bool equalTo(const Formula & f) const;
    virtual void getAtoms(AtomSet & set) const;
    virtual bool containsFormula(const Formula &f) const;
    
};

class And : public BinaryConjective {
public:
    And(const Formula & op1, const Formula & op2);
    virtual void printFormula() const;
    virtual Type getType() const;
    virtual bool eval(const Valuation & v) const;
    virtual Formula substitute(const Formula & a, const Formula & b);
    virtual Formula simplify();
    virtual Formula nnf();
    virtual void shannon();
    virtual LiteralListList listCNF();
    virtual LiteralListList listDNF();
    
};

class Or : public BinaryConjective {
public:
    Or(const Formula & op1, const Formula & op2);
    virtual void printFormula() const;
    virtual Type getType() const;
    virtual bool eval(const Valuation & v) const;
    virtual Formula substitute(const Formula & a, const Formula & b);
    virtual Formula simplify();
    virtual Formula nnf();
    virtual void shannon();
    virtual LiteralListList listCNF();
    virtual LiteralListList listDNF();
    
};

class Imp : public BinaryConjective {
public:
    Imp(const Formula & op1, const Formula & op2);
    virtual void printFormula() const;
    virtual Type getType() const;
    virtual bool eval(const Valuation & v) const;
    virtual Formula substitute(const Formula & a, const Formula & b);
    virtual Formula simplify();
    virtual Formula nnf();
    virtual void shannon();
    virtual LiteralListList listCNF();
    virtual LiteralListList listDNF();
};

class Iff : public BinaryConjective {
    
public:
    Iff(const Formula & op1, const Formula & op2);
    virtual void printFormula() const;
    virtual Type getType() const;
    virtual bool eval(const Valuation & v) const;
    virtual Formula substitute(const Formula & a, const Formula & b);
    virtual Formula simplify();
    virtual Formula nnf();
    virtual void shannon();
    virtual LiteralListList listCNF();
    virtual LiteralListList listDNF();
};


// DEFINICIJE FUNKCIJA

// FUNKCIJE ZA ODREDJIVANJE TIPA FORMULE ---------------------------------

BaseFormula::Type True::getType() const
{
    return T_TRUE;
}

BaseFormula::Type False::getType() const
{
    return T_FALSE;
}

BaseFormula::Type Atom::getType() const
{
    return T_ATOM;
}

BaseFormula::Type Not::getType() const
{
    return T_NOT;
}

BaseFormula::Type And::getType() const
{
    return T_AND;
}

BaseFormula::Type Or::getType() const
{
    return T_OR;
}

BaseFormula::Type Imp::getType() const
{
    return T_IMP;
}

BaseFormula::Type Iff::getType() const
{
    return T_IFF;
}

// --------------------------------------------------------------------

// FUNKCIJE ZA STAMPANJE FORMULA -----------------------------------------

void True::printFormula() const
{
    cout << "TRUE";
}

void False::printFormula() const
{
    cout << "FALSE";
}

void Atom::printFormula() const
{
    cout << "p_" << _var_num;
}

void Not::printFormula() const
{
    cout << "(~";
    _op->printFormula();
    cout << ")";
}

void And::printFormula() const
{
    cout << "(";
    _op1->printFormula();
    cout << "/\\ ";
    _op2->printFormula();
    cout << ")";
}

void Or::printFormula() const
{
    cout << "(";
    _op1->printFormula();
    cout << "\\/";
    _op2->printFormula();
    cout << ")";
}

void Imp::printFormula() const
{
    cout << "(";
    _op1->printFormula();
    cout << "=>";
    _op2->printFormula();
    cout << ")";
}

void Iff::printFormula() const
{
    cout << "(";
    _op1->printFormula();
    cout << "<=>";
    _op2->printFormula();
    cout << ")";
}
// -----------------------------------------------------------------------

// FUNKCIJE ZA ISPITIVANJE SINTAKSNE JEDNAKOSTI FORMULA ----------------

bool LogicConstant::equalTo(const Formula & f) const
{
    return f->getType() == getType();
}

bool Atom::equalTo(const Formula & f) const
{
    return f->getType() == T_ATOM &&
    ((Atom *)f.get())->getVariableNumber() == _var_num;
}

bool UnaryConjective::equalTo(const Formula & f) const
{
    return f->getType() == getType() &&
    _op->equalTo(((UnaryConjective *)f.get())->getOperand());
}

bool BinaryConjective::equalTo(const Formula & f) const
{
    return f->getType() == getType() &&
    _op1->equalTo(((BinaryConjective *)f.get())->getOperand1())
    &&
    _op2->equalTo(((BinaryConjective *)f.get())->getOperand2());
}

// --------------------------------------------------------------------

// FUNKCIJE ZA IZRACUNAVANJE SLOZENOSTI FORMULA ------------------------

unsigned AtomicFormula::complexity() const
{
    return 0;
}

unsigned UnaryConjective::complexity() const
{
    return _op->complexity() + 1;
}

unsigned BinaryConjective::complexity() const
{
    return _op1->complexity() + _op2->complexity() + 1;
}

// -------------------------------------------------------------------



// FUNKCIJE ZA IZDVAJANJE SKUPA ATOMA FORMULE -------------------------

void LogicConstant::getAtoms(AtomSet & set) const
{}

void Atom::getAtoms(AtomSet & set) const
{
    set.insert(_var_num);
}

void UnaryConjective::getAtoms(AtomSet & set) const
{
    _op->getAtoms(set);
}

void BinaryConjective::getAtoms(AtomSet & set) const
{
    _op1->getAtoms(set);
    _op2->getAtoms(set);
}

// -----------------------------------------------------------------------

// FUNKCIJE SUBSTITUCIJE ---------------------------------------------

Formula AtomicFormula::substitute(const Formula & a, const Formula & b)
{
    /* Kod atomickih formula, substitucija se dogadja samo
     kada je formula koju menjamo (A) sintaksno identicna
     samoj atomickoj formuli. U suprotnom, formula ostaje
     nepromenjena. Kod svih ostalih (neatomickih) formula,
     ukoliko formula koja se menja (A) nije identicki
     jednaka sa samom formulom, tada se substitucija
     primenjuje rekurzivno na podformule. */
    if(equalTo(a))
        return b;
    else
        return shared_from_this();
}

Formula Not::substitute(const Formula & a, const Formula & b)
{
    if(equalTo(a))
        return b;
    else
    {
        return Formula(new Not(_op->substitute(a, b)));
    }
    
}

Formula And::substitute(const Formula & a, const Formula & b)
{
    if(equalTo(a))
        return b;
    else
    {
        return Formula(new And(_op1->substitute(a, b), _op2->substitute(a,b)));
    }
}


Formula Or::substitute(const Formula & a, const Formula & b)
{
    if(equalTo(a))
        return b;
    else
    {
        return Formula(new Or(_op1->substitute(a, b), _op2->substitute(a,b)));
    }
    
}

Formula Imp::substitute(const Formula & a, const Formula & b)
{
    if(equalTo(a))
        return b;
    else
    {
        return Formula(new Imp(_op1->substitute(a, b), _op2->substitute(a,b)));
    }
}

Formula Iff::substitute(const Formula & a, const Formula & b)
{
    if(equalTo(a))
        return b;
    else
    {
        return Formula(new Iff(_op1->substitute(a, b), _op2->substitute(a,b)));
    }
    
}
// ----------------------------------------------------------------------

// FUNKCIJE ZA ODREDJIVANJE INTERPRETACIJE FORMULE U DATOJ VALUACIJI --

bool True::eval(const Valuation & v) const
{
    return true;
}

bool False::eval(const Valuation & v) const
{
    return false;
}

bool Atom::eval(const Valuation & v) const
{
    return v.getValue(_var_num);
}

bool Not::eval(const Valuation & v) const
{
    return !_op->eval(v);
}

bool And::eval(const Valuation & v) const
{
    return _op1->eval(v) && _op2->eval(v);
}

bool Or::eval(const Valuation & v) const
{
    return _op1->eval(v) || _op2->eval(v);
}

bool Imp::eval(const Valuation & v) const
{
    return !_op1->eval(v) || _op2->eval(v);
}

bool Iff::eval(const Valuation & v) const
{
    return _op1->eval(v) == _op2->eval(v);
}


// CONTAINS -----------
// ==================================


bool True::containsFormula(const Formula & f) const
{
    if (f->getType() == T_TRUE)
    {
        return true;
    }
    return false;
}
bool False::containsFormula(const Formula & f) const
{
    if (f->getType() == T_FALSE)
    {
        return true;
    }
    return false;
}
bool Atom::containsFormula(const Formula & f) const
{
    if (f->getType() == T_ATOM)
    {
        Atom * atom = (Atom*)(f.get());
        
        if (atom->getVariableNumber() == _var_num)
        {
            return true;
        }
        
    }
    
    return false;
}

bool Not::containsFormula(const Formula &f) const
{
    return this->getOperand()->containsFormula(f);
}

bool BinaryConjective::containsFormula(const Formula &f) const
{
    return this->_op1->containsFormula(f) || this->_op2->containsFormula(f);
}



// ---------------------------------------------------------------------


// FUNKCIJE VEZANE ZA SEMANTIKU (zadovoljivost, tautologicnost,
// logicke posledice, ekvivalencija, istinitonosne tablice...)

void BaseFormula::printTruthTable() const
{
    Valuation v;
    AtomSet atoms;
    
    /* Odredjujemo skup atoma formule */
    getAtoms(atoms);
    
    /* Kreiramo inicijalnu valuaciju za dati skup atoma (svim
     atomima se dodeljuje false vrednost inicijalno) */
    v.setInitial(atoms);
    
    /* Prolazimo kroz sve valuacije, prikazujemo valuaciju i vrednost
     formule u toj valuaciji */
    do {
        v.printValuation();
        cout << " | ";
        cout << eval(v) << endl;
        
    } while(v.nextValuation());
    
}

/* Funkcija ispituje zadovoljivost date formule.
 Preko izlaznog parametra v vraca valuaciju koja
 zadovoljava formulu, ako takva postoji */
bool BaseFormula::isSatisfiable(Valuation & v) const
{
    AtomSet atoms;
    
    getAtoms(atoms);
    
    v.setInitial(atoms);
    
    do {
        if(eval(v))
            return true;
        
    } while(v.nextValuation());
    
    return false;
}

/* Funkcija ispituje tautologicnost formule */
bool BaseFormula::isTautology() const
{
    Valuation v;
    AtomSet atoms;
    
    getAtoms(atoms);
    
    v.setInitial(atoms);
    
    do {
        if(!eval(v))
            return false;
        
    } while(v.nextValuation());
    
    return true;
}

/* Funkcija proverava da li je b logicka posledica formule a */
bool isConsequence(const Formula &  a, const Formula &  b)
{
    Valuation v;
    AtomSet atoms;
    
    a->getAtoms(atoms);
    b->getAtoms(atoms);
    
    v.setInitial(atoms);
    
    do {
        /* Ako postoji valuacija u kojoj je A tacna a B netacna... */
        if(a->eval(v) && !b->eval(v))
            return false;
        
    } while(v.nextValuation());
    
    return true;
}

bool isEquivalent(const Formula &  a, const Formula &  b)
{
    Valuation v;
    AtomSet atoms;
    
    a->getAtoms(atoms);
    b->getAtoms(atoms);
    
    v.setInitial(atoms);
    
    do {
        /* Ako postoji valuacija u kojoj formule imaju
         razlicite interpretacije */
        if(a->eval(v) != b->eval(v))
            return false;
        
    } while(v.nextValuation());
    
    return true;
}

// ---------------------------------------------------------------------

// FUNKCIJE ZA SIMPLIFIKACIJU -------------------------------------------

/* Simplifikacija atomicke formule je trivijalna */
Formula AtomicFormula::simplify()
{
    return shared_from_this();
}

Formula Not::simplify()
{
    /* Negacija se uproscava prema pravilima: ~True === False,
     ~False === True. Kod svih formula, simplifikacija se prvo primeni
     rekurzivno na podformule, pa se zatim primenjuju pravila. */
    Formula simp_op = _op->simplify();
    
    if(simp_op->getType() == T_TRUE)
        return Formula(new False());
    else if(simp_op->getType() == T_FALSE)
        return Formula(new True());
    else
        return Formula(new Not(simp_op));
}

Formula And::simplify()
{
    /* Simplifikacija konjukcije po pravilima A /\ True === A,
     A /\ False === False i sl. */
    Formula simp_op1 = _op1->simplify();
    Formula simp_op2 = _op2->simplify();
    
    if(simp_op1->getType() == T_TRUE)
        return simp_op2;
    else if(simp_op2->getType() == T_TRUE)
        return simp_op1;
    else if(simp_op1->getType() == T_FALSE ||
            simp_op2->getType() == T_FALSE)
        return Formula(new False());
    else
        return Formula(new And(simp_op1, simp_op2));
}

Formula Or::simplify()
{
    /* Simplifikacija disjunkcije po pravilima: A \/ True === True,
     A \/ False === A, i sl. */
    Formula simp_op1 = _op1->simplify();
    Formula simp_op2 = _op2->simplify();
    
    if(simp_op1->getType() == T_FALSE)
        return simp_op2;
    else if(simp_op2->getType() == T_FALSE)
        return simp_op1;
    else if(simp_op1->getType() == T_TRUE ||
            simp_op2->getType() == T_TRUE)
        return Formula(new True());
    else
        return Formula(new Or(simp_op1, simp_op2));
}

Formula Imp::simplify()
{
    /* Simplifikacija implikacije po pravilima: A ==> True === True,
     A ==> False === ~A, True ==> A === A, False ==> A === True */
    Formula simp_op1 = _op1->simplify();
    Formula simp_op2 = _op2->simplify();
    
    if(simp_op1->getType() == T_TRUE)
        return simp_op2;
    else if(simp_op2->getType() == T_TRUE)
        return Formula(new True());
    else if(simp_op1->getType() == T_FALSE)
        return Formula(new True());
    else if(simp_op2->getType() == T_FALSE)
        return Formula(new Not(simp_op1));
    else
        return Formula(new Imp(simp_op1, simp_op2));
}

Formula Iff::simplify()
{
    /* Ekvivalencija se simplifikuje pomocu pravila:
     True <=> A === A, False <=> A === ~A i sl. */
    
    Formula simp_op1 = _op1->simplify();
    Formula simp_op2 = _op2->simplify();
    
    if(simp_op1->getType() == T_FALSE &&
       simp_op2->getType() == T_FALSE)
        return Formula(new True());
    else if(simp_op1->getType() == T_TRUE)
        return simp_op2;
    else if(simp_op2->getType() == T_TRUE)
        return simp_op1;
    else if(simp_op1->getType() == T_FALSE)
        return Formula(new Not(simp_op2));
    else if(simp_op2->getType() == T_FALSE)
        return Formula(new Not(simp_op1));
    else
        return Formula(new Iff(simp_op1, simp_op2));
}

// ---------------------------------------------------------------------


// Shannon FUNKCIJE ----------------------------------------------------

vector<Formula> testVector;

void AtomicFormula::shannon()
{
    testVector.push_back(shared_from_this());
    this->printFormula();
}

void Not::shannon()
{
    
}

void And::shannon()
{

}
                         
void Or::shannon()
{
    
}

void Imp::shannon()
{
    
}

void Iff::shannon()
{
    
}

// ---------------------------------------------------------------------


// NNF FUNKCIJE --------------------------------------------------------

Formula AtomicFormula::nnf()
{
    return shared_from_this();
}

Formula Not::nnf()
{
    /* Eliminacija dvojne negacije */
    if(_op->getType() == T_NOT)
    {
        Not * not_op = (Not *) _op.get();
        return not_op->getOperand()->nnf();
    }
    /* De-Morganov zakon ~(A/\B) === ~A \/ ~B, pa zatim rekurzivna
     primena nnf-a na ~A i ~B */
    else if(_op->getType() == T_AND)
    {
        And * and_op =  (And *) _op.get();
        
        return Formula(new Or(Formula(new Not(and_op->getOperand1()))->nnf(),
                              Formula(new Not(and_op->getOperand2()))->nnf()));
        
    }
    /* De-Morganov zakon ~(A\/B) === ~A /\ ~B, pa zatim rekurzivna
     primena nnf-a na ~A i ~B */
    else if(_op->getType() == T_OR)
    {
        Or * or_op =  (Or *) _op.get();
        
        return Formula(new And(Formula(new Not(or_op->getOperand1()))->nnf(),
                               Formula(new Not(or_op->getOperand2()))->nnf()));
        
    }
    /* De-Morganov zakon ~(A==>B) === A /\ ~B, pa zatim rekurzivna
     primena nnf-a na A i ~B */
    else if(_op->getType() == T_IMP)
    {
        Imp * imp_op =  (Imp *) _op.get();
        
        return Formula(new And(imp_op->getOperand1()->nnf(),
                               Formula(new Not(imp_op->getOperand2()))->nnf()));
        
    }
    /* Primena pravila ~(A<=>B) === (A /\ ~B) \/ (B /\ ~A) */
    else if(_op->getType() == T_IFF)
    {
        Iff * iff_op =  (Iff *) _op.get();
        
        return Formula(new Or(Formula(new And(iff_op->getOperand1()->nnf(),
                                              Formula(new Not(iff_op->getOperand2()))->nnf())),
                              Formula(new And(iff_op->getOperand2()->nnf(),
                                              Formula(new Not(iff_op->getOperand1()))->nnf()))));
        
    }
    else
    {
        return shared_from_this();
    }
}

Formula And::nnf()
{
    return Formula(new And(_op1->nnf(), _op2->nnf()));
}

Formula Or::nnf()
{
    return Formula(new Or(_op1->nnf(), _op2->nnf()));
}

Formula Imp::nnf()
{
    /* Eliminacija implikacije, pa zatim rekurzivna primena nnf()-a */
    return Formula(new Or(Formula(new Not(_op1))->nnf(), _op2->nnf()));
}

Formula Iff::nnf()
{
    /* Eliminacija ekvivalencije, pa zatim rekurzivna primena nnf()-a.
     Primetimo da se ovde velicina formule duplira */
    return Formula(new And(Formula(new Or(Formula(new Not(_op1))->nnf(), _op2->nnf())),
                           Formula(new Or(Formula(new Not(_op2))->nnf(), _op1->nnf()))));
}

// ---------------------------------------------------------------------


// POMOCNE FUNKCIJE ZA BARATANJE LISTAMA --------------------------------

/* Funkcija nadovezuje dve liste */
template <typename T>
T concatLists(const T & c1, const T & c2)
{
    T c = c1;
    copy(c2.begin(), c2.end(), back_inserter(c));
    return c;
}


/* Funkcija nadovezuje svaku listu literala iz c1 sa svakom listom literala
 iz c2 i sve takve liste literala smesta u rezultujucu listu listi c */
LiteralListList makePairs(const LiteralListList & c1,
                          const LiteralListList & c2)

{
    LiteralListList c;
    
    for(LiteralListList::const_iterator it = c1.begin();
        it != c1.end(); ++it)
    {
        for(LiteralListList::const_iterator jt = c2.begin();
            jt != c2.end(); ++jt)
        {
            c.push_back(concatLists(*it, *jt));
        }
    }
    
    return c;
}

/* Funkcija prikazuje listu listi */
void printList(const LiteralListList & l)
{
    cout << "[ ";
    for(LiteralListList::const_iterator i = l.begin();
        i != l.end(); i++)
    {
        cout << "[ ";
        for(LiteralList::const_iterator j = i->begin();
            j != i->end(); j++)
        {
            (*j)->printFormula();
            cout << " ";
        }
        cout << " ] ";
    }
    cout << " ]" << endl;
    
}

#pragma mark - prebacivanje u vektor i manipulacije formulama

vector<Formula> andFormulaFromList(const LiteralListList & l)
{
    vector<Formula> ands;
    
    int length = (int)l.size();

    for(int i=0; i<length; i++)
    {
        int length2 = (int)l[i].size();
        
        
        Formula nF;
        
        if(length2 >= 2)
        {
            Formula f = Formula(new And(l[i][0],l[i][1]));
            nF = Formula(f);
            
            if (length2 >= 3)
            {
                for (int j=3; j < length2; j++)
                {
                    nF = Formula(new And(f,l[i][j]));
                    f = nF;
                }
            }
        }
        else
        {
            nF = l[i][0];
        }
        
        ands.push_back(nF);
    }
    
    return ands;
}

Formula formulaFromList(const LiteralListList & l)
{
    vector<Formula> ands = andFormulaFromList(l);
    
    if (ands.size() == 1)
    {
        return ands[0];
    }
    else
    {
        Formula or1 = Formula(new Or (ands[0],ands[1]));
        Formula finalOr = Formula(or1);
        
        if (ands.size() > 2)
        {
            for(int i=3; i<ands.size(); i++)
            {
                finalOr = Formula(new Or(or1,ands[i]));
                or1 = finalOr;
            }
        }
        
        return finalOr;
    }
    
}


Formula makePositive(const Formula & f);

set<Formula> getLiteralSet(const LiteralListList & l)
{
    set<Formula> literalSet;
    
    int length = (int)l.size();
    
    for(int i=0; i<length; i++)
    {
        int length2 = (int)l[i].size();
        
        for(int j=0; j<length2; j++)
        {
            Formula f = makePositive(l[i][j]);
            literalSet.insert(f);
        }
    }

    return literalSet;
}

Formula makePositive(const Formula & f)
{
    if (f->getType() == BaseFormula::T_NOT)
    {
        return ((Not*)f.get())->getOperand();
        
    }
    
    return f;
}


// ----------------------------------------------------------------------


// FUNKCIJE ZA ODREDJIVANJE CNF U OBLIKU LISTE LISTA LITERALA ----------

LiteralListList True::listCNF()
{
    /* Formuli True odgovara prazna lista klauza. Naime, po konvenciji,
     prazna lista klauza je zadovoljena u svakoj valuaciji (zato sto
     u skupu ne postoji klauza koja nije zadovoljena), pa je otuda
     logicki ekvivalentna formuli True. */
    LiteralListList cl;
    return cl;
}

LiteralListList False::listCNF()
{
    /* Formuli False odgovara lista klauza koja sadrzi samo jednu praznu
     klauzu. Po konvenciji, prazna klauza je netacna u svakoj valuaciji,
     (zato sto ne postoji literal koji je zadovoljen), pa je otuda
     logicki ekvivalentna sa False */
    LiteralListList cl;
    LiteralList c;
    cl.push_back(c);
    return cl;
}

LiteralListList Atom::listCNF()
{
    /* Pozitivan literal (atom) se predstavlja listom klauza koja sadrzi
     samo jednu klauzu koja se sastoji iz tog jednog literala */
    LiteralListList cl;
    LiteralList c;
    c.push_back(shared_from_this());
    cl.push_back(c);
    return cl;
}

LiteralListList Not::listCNF()
{
    /* Negativan literal se predstavlja listom klauza koja sadrzi
     samo jednu klauzu koja se sastoji iz tog jednog literala */
    LiteralListList cl;
    LiteralList c;
    c.push_back(shared_from_this());
    cl.push_back(c);
    return cl;
}

LiteralListList And::listCNF()
{
    /* CNF lista se kod konjukcije dobija nadovezivanjem CNF listi
     podformula */
    LiteralListList cl1 = _op1->listCNF();
    LiteralListList cl2 = _op2->listCNF();
    
    
    return concatLists(cl1, cl2);
}

LiteralListList Or::listCNF()
{
    /* CNF lista disjunkcije se dobija tako sto se CNF liste podformula
     distributivno "pomnoze", tj. liste literala se nadovezu svaka sa
     svakom */
    
    LiteralListList cl1 = _op1->listCNF();
    LiteralListList cl2 = _op2->listCNF();
    
    return makePairs(cl1, cl2);
}

LiteralListList Imp::listCNF()
{
    throw "CNF not aplicable";
}

LiteralListList Iff::listCNF()
{
    throw "CNF not aplicable";
}

// -------------------------------------------------------------------

// FUNKCIJE ZA ODREDJIVANJE DNF U OBLIKU LISTE LISTA LITERALA ----------

LiteralListList True::listDNF()
{
    /* Formuli True odgovara lista koja sadrzi samo jednu praznu listu
     literala. Po konvenciji, prazna konjunkcija je tacna u svakoj valuaciji,
     (zato sto ne postoji literal koji je nezadovoljen), pa je otuda
     logicki ekvivalentna sa True */
    LiteralListList cl;
    LiteralList c;
    cl.push_back(c);
    return cl;
}


LiteralListList False::listDNF()
{
    /* Formuli False odgovara prazna lista listi. Naime, po konvenciji,
     prazna lista listi je nezadovoljena u svakoj valuaciji (zato sto
     u skupu ne postoji lista literala koja je zadovoljena), pa je otuda
     logicki ekvivalentna formuli False. */
    LiteralListList cl;
    return cl;
}

LiteralListList Atom::listDNF()
{
    /* Pozitivan literal (atom) se predstavlja listom koja sadrzi
     samo jednu listu koja se sastoji iz tog jednog literala */
    LiteralListList cl;
    LiteralList c;
    c.push_back(shared_from_this());
    cl.push_back(c);
    return cl;
}

LiteralListList Not::listDNF()
{
    /* Negativan literal se predstavlja listom koja sadrzi
     samo jednu listu koja se sastoji iz tog jednog literala */
    LiteralListList cl;
    LiteralList c;
    c.push_back(shared_from_this());
    cl.push_back(c);
    return cl;
}

LiteralListList And::listDNF()
{
    /* DNF lista konjunkcije se dobija tako sto se DNF liste podformula
     distributivno "pomnoze", tj. liste literala se nadovezu svaka sa
     svakom */
    
    LiteralListList cl1 = _op1->listDNF();
    LiteralListList cl2 = _op2->listDNF();
    
    return makePairs(cl1, cl2);
}

LiteralListList Or::listDNF()
{
    /* DNF lista se kod disjunkcije dobija nadovezivanjem DNF listi
     podformula */
    LiteralListList cl1 = _op1->listDNF();
    LiteralListList cl2 = _op2->listDNF();
    
    return concatLists(cl1, cl2);
}


LiteralListList Imp::listDNF()
{
    /*
     DNF implikacije je a=>b == (a/\b)\/(~a/\b)\/(~a/\~b)
     */
    
    Formula and1 = Formula(new And(_op1,_op2));
    Formula and2 = Formula(new And(Formula(new Not(_op1)), _op2));
    Formula and3 = Formula(new And(Formula(new Not(_op1)),Formula(new Not(_op2))));
    
    
    LiteralListList cl1 = and1->listDNF();
    LiteralListList cl2 = and2->listDNF();
    LiteralListList cl3 = and3->listDNF();
    
    return concatLists(concatLists(cl1, cl2), cl3);
    
}

LiteralListList Iff::listDNF()
{
    /*
     DNF ekvivaliencije je a<=>b == (a/\b)\/(~a/\~b)
     */
    Formula and1 = Formula(new And(_op1,_op2));
    Formula and3 = Formula(new And(Formula(new Not(_op1)),Formula(new Not(_op2))));
    
    
    LiteralListList cl1 = and1->listDNF();
    LiteralListList cl3 = and3->listDNF();
    
    return concatLists(cl1, cl3);

}

// -------------------------------------------------------------------


// OSTALE FUNKCIJE (konstruktori, get-eri...)

Atom::Atom(unsigned num)
:_var_num(num)
{}

unsigned Atom::getVariableNumber() const
{
    return _var_num;
}

UnaryConjective::UnaryConjective(const Formula & op)
:_op(op)
{}

const Formula & UnaryConjective::getOperand() const
{
    return _op;
}

Not::Not(const Formula & op)
:UnaryConjective(op)
{}

BinaryConjective::BinaryConjective(const Formula & op1, const Formula & op2)
:_op1(op1),
_op2(op2)
{}

const Formula & BinaryConjective::getOperand1() const
{
    return _op1;
}

const Formula & BinaryConjective::getOperand2() const
{
    return _op2;
}

And::And(const Formula & op1, const Formula & op2)
:BinaryConjective(op1, op2)
{}

Or::Or(const Formula & op1, const Formula & op2)
:BinaryConjective(op1, op2)
{}

Imp::Imp(const Formula & op1, const Formula & op2)
:BinaryConjective(op1, op2)
{}

Iff::Iff(const Formula & op1, const Formula & op2)
:BinaryConjective(op1, op2)
{}

// DEFINICIJE FUNKCIJA CLANICA KLASE Valuation ---------------------------

void Valuation::setInitial(const AtomSet & atoms)
{
    AtomSet::const_iterator it = atoms.begin(), it_end = atoms.end();
    
    _map.clear();
    for(; it != it_end; ++it)
        _map.insert(make_pair(*it, false));
}

bool Valuation::nextValuation()
{
    map<unsigned, bool>::reverse_iterator it = _map.rbegin(),
    it_end = _map.rend();
    
    for(; it != it_end; ++it)
    {
        it->second = !it->second;
        if(it->second == true)
            return true;
    }
    return false;
}

bool Valuation::getValue(unsigned p) const
{
    map<unsigned, bool>::const_iterator it = _map.find(p);
    if(it == _map.end())
        throw "Unknown variable";
    else
        return it->second;
}

void Valuation::setValue(unsigned p, bool b)
{
    _map[b] = b;
}

void Valuation::printValuation() const
{
    map<unsigned, bool>::const_iterator it = _map.begin(),
    it_end = _map.end();
    
    for(; it != it_end; ++it)
    {
        cout << it->second << " ";
    }
}
// -----------------------------------------------------------------------

Formula getNextLiteral(Formula &f, set<Formula> & literalSet)
{
    
    for (set<Formula>::iterator it = literalSet.begin(); it!=literalSet.end(); ++it)
    {
        Formula itFormula = *it;
        if (f->containsFormula(itFormula))
        {
            return itFormula;
            //it = literalSet.end();
            
        }
    }
    return nullptr;
}



bool yes = true;
bool no = false;

void makeTreeBDD(binary_tree & tree, Formula & f, set<Formula> & literalSet, int & index, int * level, bool isLeft = false)
{
    
    Formula node = getNextLiteral(f, literalSet);
    
    int var_num;
    
    if (!node)
    {
        
        (*level)++;
        
        if (f->getType() == BaseFormula::T_TRUE)
        {
            tree.insertBool(&yes);
            cout << "TRUE at " << &yes << endl;
        }
        else if (f->getType() == BaseFormula::T_FALSE)
        {
            tree.insertBool(&no);
            cout << "FALSE at " << &no << endl;
        }
        
        if (*level == 2)
        {
            tree.updateRootHistory();
            *level = 0;
        }
        
        return;
    }
    else
    {
        *level = 0;
        var_num = ((Atom*) node.get())->getVariableNumber();
        var_num++;
        node->printFormula();
        cout << endl;f->printFormula();
        cout << "\n";
        
        if (isLeft) { cout << "Adding " << -var_num <<endl;tree.insert(-var_num);}
        else {cout << "Adding " << var_num <<endl;tree.insert(var_num);};
        tree.print();cout<<endl;
    }
    
    
    Formula yesF = Formula(new True);
    Formula noF = Formula(new False);
    
    Formula left = f->substitute(node, noF)->simplify();
    Formula right = f->substitute(node, yesF)->simplify();
    
    makeTreeBDD(tree, left, literalSet, index,  level, true);
    
    
    makeTreeBDD(tree, right, literalSet, index, level, false );
    
}

int main()
{
    /*************************
     ATOMS
     *************************/
    Formula p0 = Formula(new Atom(0));
    Formula p1 = Formula(new Atom(1));
    Formula p2 = Formula(new Atom(2));
    
    
    /*************************
     BDD test primer
     *************************/
    Formula imp = Formula(new Imp(p0,p1)); // p0->p1
    Formula eq = Formula(new Iff(p1,p2)); // p1<->p2
    Formula andF = Formula(new And(imp,eq)); // p0->p1 && p1<->p2
    
    //prevodjenje formule u DNF
    LiteralListList list = andF->listDNF();

    //printList(list);
    
    //iz DNF liste listi nazad u formulu
    Formula toBDD = formulaFromList(list); // (po p1 p2) (p0 p1 ~p2) (~p0 p1 ~p2) (~p0 ~p1 p2) (~p0 ~p1 ~p2);
    
    /*************************
     BDD WIKI primer
     *************************/
    Formula notP0 = Formula(new Not(p0));
    Formula notP1 = Formula(new Not(p1));
    Formula notP2 = Formula(new Not(p2));
    
    Formula and1 = Formula(new And(notP0,Formula(new And(notP1,notP2)))); // ~p0 && ~p1 && ~p2
    Formula and2 = Formula(new And(p0,p1)); // p1 && p2
    Formula and3 = Formula(new And(p1,p2)); // p2 && p3
    Formula toBDD2 = Formula(new Or(and1,Formula(new Or(and2,and3)))); // and1 || and2 || and3
    
    /*************************
     BDD CREATOR SETUP
     *************************/
    binary_tree * tree = new binary_tree;
    int index = 0;
    set<Formula> sf = getLiteralSet(list);
    vector<Formula> usedLiterlas = vector<Formula>();
    int deadEndCounter = 0;

    /*************************
     ORIGINAL FORMULA
     *************************/
    cout << "Original formula\n";
    toBDD2->printFormula();
    cout << "\n---------------------------------\n";
    
    /*************************
     TREE CREATION
     *************************/
    //makeTreeBDD(*tree, toBDD, sf, index, &deadEndCounter);
    makeTreeBDD(*tree, toBDD2, sf, index, &deadEndCounter);
    tree->print();

    /*************************
     EVAL VALUATION
     *************************/
    vector<int> val = {-1,2,-3};
    bool isRoot = false;
    tree->searchValuation(val, &isRoot, nullptr);
    cout << "\n";
    
    return 0;
}